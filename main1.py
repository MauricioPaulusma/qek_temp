from tkinter import *
import tkinter.font as font
from typing import Optional, Callable


class Application:
    def __init__(self, master=None):
        self.root = Tk()
        self.root.wm_title('Hello world!')
        self.root.geometry("1480x320")
        self.root.overrideredirect(True)
        self.root.wm_resizable(False, False)

        self.font = font.Font(family='Helvatica', size=24)

        self.button_image = PhotoImage(file='Images/Button.png')
        self.frames = []
        self.cur_frame_index = 0

        self.Create_Frames()

        self.root.mainloop()

    def Create_Frames(self):
        self.frames.append(Frame(self.root))
        self.frames.append(Frame(self.root))
        self.frames.append(Frame(self.root))

        self.Make_Grid(self.frames[0], [
            [('Menu', lambda: Call_Action('Menu')), None, ('Help', lambda: Call_Action('Help'))],
            [None, ('Fun0', lambda: Call_Action('Fun0')), None],
            [('Prev', self.Prev_Page), (f'Page {1}/{len(self.frames)}', None), ('Next', self.Next_Page)],
        ])

        self.Make_Grid(self.frames[1], [
            [None, None, None],
            [None, None, None],
            [('Prev', self.Prev_Page), (f'Page 2/{len(self.frames)}', None), ('Next', self.Next_Page)]
        ])

        self.Make_Grid(self.frames[2], [
            [None, None, None],
            [None, None, None],
            [('Prev', self.Prev_Page), (f'Page 3/{len(self.frames)}', None), ('Next', self.Next_Page)]
        ])

        self.frames[0].pack()

    def Make_Grid(self, parent, grid):
        for row_nr in range(len(grid)):
            for col_nr in range(len(grid[row_nr])):
                if grid[row_nr][col_nr] is None:
                    frame = Frame(parent, width=self.button_image.width(), height=self.button_image.height())
                    frame.grid(row=row_nr, column=col_nr)
                    print(f'Frame is {frame.winfo_width()}x{frame.winfo_height()}')
                else:
                    (text, command) = grid[row_nr][col_nr]
                    if command is None:
                        Label(parent, text=text, font=self.font).grid(row=row_nr, column=col_nr)
                    else:
                        but = Button(parent, image=self.button_image, text=text, compound='center', font=self.font,
                                     command=command, borderwidth=0)
                        but.grid(row=row_nr, column=col_nr)
                        print(f'Button is {but.winfo_width()}x{but.winfo_height()}')

    def Next_Page(self):
        self.frames[self.cur_frame_index].pack_forget()
        if self.cur_frame_index + 1 == len(self.frames):
            self.cur_frame_index = 0
        else:
            self.cur_frame_index = self.cur_frame_index + 1

        self.frames[self.cur_frame_index].pack()

    def Prev_Page(self):
        self.frames[self.cur_frame_index].pack_forget()
        if self.cur_frame_index - 1 == -1:
            self.cur_frame_index = len(self.frames) - 1
        else:
            self.cur_frame_index = self.cur_frame_index - 1

        self.frames[self.cur_frame_index].pack()


def Call_Action(action):
    print(f'Action called {action}!')


# Call_Action("Hi there")

app = Application()
