import RPi.GPIO as GPIO # library for the controlling the GPIO
from time import sleep

class NumPad:
	
	# Dictionary for linking the pins of the number pad connector to BCM GPIO numbers.
	# This layer allows to link the pins of the number pad connector to the GPIO of the BCM chip on the raspberry pi
	io = {	
		"pin_1" : 22,	# Read line (collumns)
		"pin_2" : 27,	# Read line (collumns)
		"pin_3" : 17,	# Read line (collumns)
		"pin_4" : 26,    # This io (GPIO0) is also connected to the i2c line on the pi (for communicating with the EEPROM)
		"pin_5" : 19,	# Read line (collumns)
		"pin_6" : 13,	# Read line (collumns)
		"pin_7" : 6,	# Read line (collumns)
		"pin_8" : 5,	# Read line (collumns)
		"pin_9" : 25,	# Read line (collumns)
		"pin_10" : 11 ,	# Read line (collumns) # Patched
		"pin_11" : 9,	# Read line (collumns) # Patched
		"pin_12" : 10,	# Read line (collumns) # Patched
		"pin_13" : 21,	# Drive line (rows)
		"pin_14" : 20,	# Drive line (rows)
		"pin_15" : 16,	# Drive line (rows)
	}

	
	# List for linking the switches of the number pad to pins on the number pad connector.
	# The first element is the pin that drives the switch, the second element is the pin
	# from which the driving voltage is read. Added this layer so it is easier debugging and 
	# also to be able to use this library for other num pads.
	switches = [ 
		[ io["pin_13"],io["pin_12"] ],	#sw1
		[ io["pin_13"],io["pin_11"] ],	#sw2 	
		[ io["pin_13"],io["pin_10"] ],	#sw3
		[ io["pin_13"],io["pin_9"]  ],	#sw4
		[ io["pin_13"],io["pin_8"]  ],	#sw5 	
		[ io["pin_13"],io["pin_7"]  ],	#sw6
		[ io["pin_13"],io["pin_6"]  ],	#sw7
		[ io["pin_13"],io["pin_5"]  ],	#sw8 	
		[ io["pin_13"],io["pin_4"]  ],	#sw9
		[ io["pin_13"],io["pin_3"]  ],	#sw10
		[ io["pin_13"],io["pin_2"]  ],	#sw11	
		[ io["pin_13"],io["pin_1"]  ],	#sw12
		[ io["pin_14"],io["pin_12"] ],	#sw13
		[ io["pin_14"],io["pin_11"] ],	#sw14	
		[ io["pin_14"],io["pin_10"] ],	#sw15
		[ io["pin_14"],io["pin_9"]  ],	#sw16
		[ io["pin_14"],io["pin_8"]  ],	#sw17 	
		[ io["pin_14"],io["pin_7"]  ],	#sw18
		[ io["pin_14"],io["pin_6"]  ],	#sw19
		[ io["pin_14"],io["pin_5"]  ],	#sw20	
		[ io["pin_14"],io["pin_4"]  ],	#sw21
		[ io["pin_14"],io["pin_3"]  ],	#sw22
		[ io["pin_14"],io["pin_2"]  ],	#sw23	
		[ io["pin_14"],io["pin_1"]  ],	#sw24
		[ io["pin_15"],io["pin_12"] ],	#sw25
		[ io["pin_15"],io["pin_11"] ],	#sw26	
		[ io["pin_15"],io["pin_10"] ],	#sw27
		[ io["pin_15"],io["pin_9"]  ],	#sw28
		[ io["pin_15"],io["pin_8"]  ],	#sw29	
		[ io["pin_15"],io["pin_7"]  ],	#sw30 
		[ io["pin_15"],io["pin_6"]  ],	#sw31
		[ io["pin_15"],io["pin_5"]  ],	#sw32	
		[ io["pin_15"],io["pin_4"]  ],	#sw33
		[ io["pin_15"],io["pin_3"]  ],	#sw34
		[ io["pin_15"],io["pin_2"]  ],	#sw35	
		[ io["pin_15"],io["pin_1"]  ],	#sw36   
				] 
					
	
	def __init__(self):
		
		# Configure the GPIO library to use indexing of the GPIO by the BCM io chip numbering
		GPIO.setmode(GPIO.BCM) 
		GPIO.setwarnings(False)
		
		# configure the collum pins (read lines) as inputs  
		GPIO.setup(17, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
		GPIO.setup(27, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
		GPIO.setup(22, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
		GPIO.setup(10, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
		GPIO.setup(9,  GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
		GPIO.setup(11, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
		GPIO.setup(25,  GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
		GPIO.setup(5,  GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
		GPIO.setup(6,  GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
		GPIO.setup(13, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
		GPIO.setup(19, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
		GPIO.setup(26, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

		# configure the row pins (drive lines) as outputs
		GPIO.setup(16, GPIO.OUT)
		GPIO.output(16, GPIO.LOW)

		GPIO.setup(20, GPIO.OUT)
		GPIO.output(20, GPIO.LOW)

		GPIO.setup(21, GPIO.OUT)
		GPIO.output(21, GPIO.LOW)
		
	def scanpad(self):
		i = 0
		delay = 0.0005 # we need to add a bit of delay between driving the GPIO output and reading the GPIO input. This is because there is a bit of delay in the io line.  
		for switch in self.switches:
			i = i + 1
			GPIO.output(switch[0], GPIO.HIGH)
			sleep(delay)
			if GPIO.input(switch[1]):
				GPIO.output(switch[0], GPIO.LOW)
				return i
			GPIO.output(switch[0], GPIO.LOW)
			sleep(delay)		
		return 0
