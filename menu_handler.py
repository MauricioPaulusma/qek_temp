import menu
import tkinter


class MenuHandler:

    def __init__(self, root):
        self.main_frame = tkinter.Frame(root)

        self.main_menu = menu.Menu(self.main_frame, [
            ['CHECK\nFIRE ON', 'INHIBIT\nFIRE'],
            ['BREAK\nENGAGE', 'MON TRK'],
            [None, "Delete"]])
        self.main_menu.GetFrame().grid(row=0, column=0, rowspan=5, columnspan=2, sticky=tkinter.NSEW)

        self.menu_items = menu.Menu(self.main_frame, [['GEN', 'TRACK AIR', 'TRACK\nSURF', 'TRACK\nSUB', 'TRACK\nLAND', 'SMART-S\nENGAGE', 'TRACKER\nENGAGE', 'WEAPON\nENGAGE', 'EW', 'LINK']], True)
        self.menu_items.GetFrame().grid(row=0, column=2, rowspan=2, columnspan=10, sticky=tkinter.NSEW)

        self.menu_frame = tkinter.Frame(self.main_frame)
        self.menu_frame.grid(row=2, column=2, rowspan=3, columnspan=9, sticky=tkinter.NSEW)

        self.gen_menu = menu.Menu(self.menu_frame, [
            ['ASSOC\nTRK', 'SENS RNG\nAIR', 'WPN RNG\nAIR', 'ECM BRN', 'R-ESM\nBRN AIR', 'JOIN', None, None, None, 'SELECT\nWECDIS'],
            ['DISASS\nTRK', 'SENS RNG\nNON-AIR', 'WPN RNG\nNON-AIR', 'ENG\nADVICE', 'C-ESM BRN\nAIR', 'CHECK\nFIREOFF', None, 'INIT\nICM', 'DRCP', 'DESELECT\nWECDIS']])
        self.gen_menu.GetFrame().grid(row=0, column=0, sticky=tkinter.NSEW)

        self.track_air_menu = menu.Menu(self.menu_frame, [
            ['ASSOC\nTRK', 'INIT\nRAMAIR', None, None, 'ECM BRN\nAIR', 'R-ESM\nBRN AIR', 'SMART-S\nDEL', None, 'INIT\nAREA', 'MARK\nESS'],
            ['DISASS\nTRK', 'SENS RNG\nNON-AIR', 'WPN RNG\nNON-AIR', 'ENG\nADVICE', 'C-ESM BRN\nAIR', 'CHECK\nFIREOFF', None, 'INIT\nICM', 'DRCP', 'DESELECT\nWECDIS']])
        self.track_air_menu.GetFrame().grid(row=0, column=0, sticky=tkinter.NSEW)

        self.track_surf_menu = menu.Menu(self.menu_frame, [
            ['INIT\nRAMSRF', None, None, 'ECM\nBRN SRF', 'R-ESM\nBRN SRF', 'INIT NRT\nBRNSRF', 'INIT ACO\nBRNSRF', None, 'INIT\nAREA', 'MARK\nESS'],
            ['INIT NRT\nPTSRF', 'SMART\nSURF', 'IFF\nCHALL', 'INIT\nLINE', 'C-ESM\nBRN SRF', 'SHIFT', 'REPOS', 'UPDATE', 'INIT\nAW SP', 'UNMARK\nESS']])
        self.track_surf_menu.GetFrame().grid(row=0, column=0, sticky=tkinter.NSEW)

        self.track_sub_menu = menu.Menu(self.menu_frame, [
            [None, 'INIT ASW\nAOP', 'INIT ASW\nFIX', 'ECM\nBRN SUBS', 'R-ESM\nBRN SUBS', 'INIT NRT\nBURNSUB', 'INIT NRT\nBRNSUB', None, 'INIT\nAREA', 'MARK\nESS'],
            ['INIT NRT\nPTSUB', None, 'INIT\nDATUM', 'INIT\nLINE', 'C-ESM\nBRN SUBS', None, None, None, 'INIT\nASW', 'UNMARK\nESS']])
        self.track_sub_menu.GetFrame().grid(row=0, column=0, sticky=tkinter.NSEW)

        self.track_land_menu = menu.Menu(self.menu_frame, [
            [None, None, None, 'ECN\nBRN LND', 'R-ESM\nBRN LND', 'INIT NRT\nBRNLND', None, None, 'INIT\nAREA', 'MARK\nESS'],
            ['INIT NRT\nPTLND', None, None, 'INIT\nLINE', 'C-ESM\nBRN LND', None, None, None, 'INIT\nPTLND', 'UNMARK\nESS']])
        self.track_land_menu.GetFrame().grid(row=0, column=0, sticky=tkinter.NSEW)

        self.track_smart_s_engage_menu = menu.Menu(self.menu_frame, [
            ['DESIG\nTWS-1', 'DESIG\nTWS-2', 'DESIG\nTWS-3', None, None, None, None, None, None, 'MISS'],
            ['BREAK\nTWS-1', 'BREAK\nTWS-2', 'BREAK\nTWS-3', 'ENG\nADVICE', 'KILL', None, None, None, None, None]])
        self.track_smart_s_engage_menu.GetFrame().grid(row=0, column=0, sticky=tkinter.NSEW)

        self.tracker_engage_menu = menu.Menu(self.menu_frame, [
            ['DESIG\nMIRADOR', 'SLAVE\nMIRADOR', 'SS\nMIRADOR', 'SRV\nMIRADOR', 'SFG\nMIRADOR', 'DESIG\nSTIR', 'SLAVE\nSTIR', 'SS\nSTIR', 'SRV\nSTIR', 'MISS'],
            ['BREAK\nMIRADOR', 'JOY TO\nMIRADOR', 'AUTOTRK\nMIRADOR', 'ENG\nADVICE', 'KILL', 'BREAK\nSTIR', 'JOY TO\nSTIR', 'AUTOTRK\nSTIR', 'RANGE\nFAR', 'RANGE\nNEAR']])
        self.tracker_engage_menu.GetFrame().grid(row=0, column=0, sticky=tkinter.NSEW)

        self.weapon_engage_menu = menu.Menu(self.menu_frame, [
            ['DESIG\n57MM', None, 'Excl\nAAW/EW', None, 'RAM\nDESIG', 'DESIG\n30MM P', None, 'Excl\nASuW', None, 'DESIG\n30MM STB'],
            ['BREAK\n57MM', None, 'Incl\nAAW/EW', 'ENG\nADVICE', 'RAM\nBREAK', 'BREAK\n30MM P', None, 'Incl\nASuW', None, 'BREAK\n30MM STB']])
        self.weapon_engage_menu.GetFrame().grid(row=0, column=0, sticky=tkinter.NSEW)

        self.ew_menu = menu.Menu(self.menu_frame, [
            [None, None, 'Excl\nAAW/EW', None, 'DROP\nR-ESM', None, None, None, None, None],
            [None, None, 'Incl\nAAW/EW', None, None, None, None, None, None, None]])
        self.ew_menu.GetFrame().grid(row=0, column=0, sticky=tkinter.NSEW)

        self.link_menu = menu.Menu(self.menu_frame, [
            ['LINK\nTRANSMIT', 'CORREL', None, 'START\nEMERG', 'START\nF-TELL', None, None, None, None, None],
            ['LINK\nDROP', 'DECORELL', None, 'STOP\nEMERG', 'STOP\nF-TELL', 'LINK\nPOINTER', None, None, None, None]])
        self.link_menu.GetFrame().grid(row=0, column=0, sticky=tkinter.NSEW)

        self.menus = [self.gen_menu, self.track_air_menu, self.track_surf_menu, self.track_sub_menu, self.track_land_menu, self.track_smart_s_engage_menu, self.tracker_engage_menu,
                      self.weapon_engage_menu, self.ew_menu, self.link_menu]

        self.main_frame.pack()

    def switch_main_menu(self, menu_index):
        self.main_menu.DeactivateAll()
        self.main_menu.ToggleButton(menu_index[0], menu_index[1])
#        self.menus[0].GetFrame().tkraise()

    def switch_menu_item(self, menu_index):
        self.menu_items.DeactivateAll()
        self.menu_items.ToggleButton(menu_index[0], 0)
        self.menus[menu_index[0]].GetFrame().tkraise()
    
    def switch_menu_item_sub(self, menu_index, active_menu_item):
        if active_menu_item == (0,0):
            self.gen_menu.DeactivateAll()
            self.gen_menu.ToggleButton(menu_index[0], menu_index[1])
            self.menus[0].GetFrame().tkraise()
        elif active_menu_item == (1,0):
            self.track_air_menu.DeactivateAll()
            self.track_air_menu.ToggleButton(menu_index[0], menu_index[1])
            self.menus[1].GetFrame().tkraise()
        elif active_menu_item == (2,0):
            self.track_surf_menu.DeactivateAll()
            self.track_surf_menu.ToggleButton(menu_index[0], menu_index[1])
            self.menus[2].GetFrame().tkraise()
        elif active_menu_item == (3,0):
            self.track_sub_menu.DeactivateAll()
            self.track_sub_menu.ToggleButton(menu_index[0], menu_index[1])
            self.menus[3].GetFrame().tkraise()
        elif active_menu_item == (4,0):
            self.track_land_menu.DeactivateAll()
            self.track_land_menu.ToggleButton(menu_index[0], menu_index[1])
            self.menus[4].GetFrame().tkraise()
        elif active_menu_item == (5,0):
            self.track_smart_s_engage_menu.DeactivateAll()
            self.track_smart_s_engage_menu.ToggleButton(menu_index[0], menu_index[1])
            self.menus[5].GetFrame().tkraise()
        elif active_menu_item == (6,0):
            self.tracker_engage_menu.DeactivateAll()
            self.tracker_engage_menu.ToggleButton(menu_index[0], menu_index[1])
            self.menus[6].GetFrame().tkraise()
        elif active_menu_item == (7,0):
            self.weapon_engage_menu.DeactivateAll()
            self.weapon_engage_menu.ToggleButton(menu_index[0], menu_index[1])
            self.menus[7].GetFrame().tkraise()
        elif active_menu_item == (8,0):
            self.ew_menu.DeactivateAll()
            self.ew_menu.ToggleButton(menu_index[0], menu_index[1])
            self.menus[8].GetFrame().tkraise()
        elif active_menu_item == (9,0):
            self.link_menu.DeactivateAll()
            self.link_menu.ToggleButton(menu_index[0], menu_index[1])
            self.menus[9].GetFrame().tkraise()
        else:
            pass
        
            
        
