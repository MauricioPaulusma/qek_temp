import string
import tkinter
import button


class Menu:
    def __init__(self, parent, grid, extra_row=False):

        self.rows = (len(grid) - 1) * 2 + 1
        self.columns = len(grid[0])

        if extra_row:
            self.rows = self.rows + 1

        self.tiles = [[] for _ in range(self.rows)]
        self.enabled = [[False] for _ in range(self.rows)]

        self.frame = tkinter.Frame(parent)
        self.tile_width = int(1480 / 12)
        self.tile_height = int(320 / 5)

        for j in range(self.rows):
            for i in range(self.columns):
                frame = tkinter.Frame(self.frame, width=self.tile_width, height=self.tile_height)
                self.tiles[j].append(frame)

        for j in range(len(grid)):
            for i in range(len(grid[j])):
                if grid[j][i] is not None:
                    self.tiles[j * 2][i] = button.Button(self.frame, self.tile_width, self.tile_height, grid[j][i],
                                                         '#FFFFFF', '#00FF00')

        self.Grid()

    def AddButton(self, i: int, j: int, text: string, off_col: string = '#ffffff', on_col: string = '#00ff00',
                  active: bool = False):
        if not self.InRange(i, j):
            return

        self.tiles[j][i] = button.Button(self.frame, self.tile_width, self.tile_height, text, off_col, on_col)
        if active:
            self.tiles[j][i].Activate()

    def Grid(self):
        for j in range(self.rows):
            for i in range(self.columns):
                self.tiles[j][i].grid(row=j, column=i, sticky=tkinter.NSEW)

    def Create(self, i: int, j: int):
        # self.frame.pack()
        self.frame.grid(row=j, column=i, rowspan=self.rows, columnspan=self.columns, sticky=tkinter.NSEW)

    def GetFrame(self):
        return self.frame

    def InRange(self, i: int, j: int):
        if i < 0 or i >= self.columns:
            print(f'Index out of range: {i} is not in range of {self.columns}!')
            return False
        if j < 0 or j >= self.rows:
            print(f'Index out of range: {j} is not in range of {self.rows}!')
            return False

        return True

    def DeactivateAll(self):
        for i in self.tiles:
            for j in i:
                if isinstance(j, button.Button):
                    j.Deactivate()

    def ToggleButton(self, i: int, j: int):
        if not self.InRange(i, j):
            return
        if isinstance(self.tiles[j][i], button.Button):
            self.tiles[j][i].Toggle()
        else:
            print(f'{i}, {j} is not a button!')
