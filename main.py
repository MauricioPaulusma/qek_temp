from time import sleep
import tkinter # library for creating the gui
from threading import Thread # library for creating multiple threads
import serial
import time
import numpad
import menu 
from menu_handler import MenuHandler
from numpad import NumPad
import tty, sys, termios

num_pad = NumPad()


active_item = (0,0)

# function to link a numpad switch to a menu button
def numpad_to_menu(numpad):
    # list that links a numpad button a menu button (yes, bit of an ugly way, but figured this is the easiest tunable/debugable way) 
    menu = [
        ["main",(0,0)], 
        ["main",(1,0)], 
        ["item",(0,0)],
        ["item",(1,0)],
        ["item",(2,0)],
        ["item",(3,0)],
        ["item",(4,0)],
        ["item",(5,0)],
        ["item",(6,0)],
        ["item",(7,0)],
        ["item",(8,0)],
        ["item",(9,0)],
        ["main",(0,2)],
        ["main",(1,2)],
        ["item_sub",(0,0)],
        ["item_sub",(1,0)],
        ["item_sub",(2,0)],
        ["item_sub",(3,0)],
        ["item_sub",(4,0)],
        ["item_sub",(5,0)],
        ["item_sub",(6,0)],
        ["item_sub",(7,0)], 
        ["item_sub",(8,0)],
        ["item_sub",(9,0)],
        ["main",(0,4)],
        ["main",(1,4)],
        ["item_sub",(0,2)],
        ["item_sub",(1,2)],
        ["item_sub",(2,2)],
        ["item_sub",(3,2)],
        ["item_sub",(4,2)],
        ["item_sub",(5,2)],
        ["item_sub",(6,2)],
        ["item_sub",(7,2)], 
        ["item_sub",(8,2)],
        ["item_sub",(9,2)]
    ]
    
    
    return menu[numpad-1]


def switch_poll():
    global active_item
    while True:
        time.sleep(0.001)
        pressed_key = num_pad.scanpad()
        if pressed_key:
#            print("swtich ", pressed_key, " is pressed")
            menu = numpad_to_menu(pressed_key) # translate the pressed key to the menu button 
#            print("menu ", menu, " activated")
            if menu[0] == "main": 
                menu_handler.switch_main_menu(menu[1])
            elif menu[0] == "item":
                active_item = menu[1]
                menu_handler.switch_menu_item(menu[1])
            elif menu[0] == "item_sub":
                print("menu[1]= ", menu[1], " active_item = ", active_item)
                menu_handler.switch_menu_item_sub(menu[1], active_item)
#            ser.write(0xAA) # write a test message to the UART
          

def kill_poll():
    while True:
        time.sleep(0.01)
        filedescriptors = termios.tcgetattr(sys.stdin)
        tty.setcbreak(sys.stdin)
        x = 0
        x=sys.stdin.read(1)[0]
        print("You pressed", x)
        if x == "r":
            print("If condition is met")
        termios.tcsetattr(sys.stdin, termios.TCSADRAIN, filedescriptors)
            

root = tkinter.Tk()
root.wm_title('Hello world!')
root.geometry("1480x320")



screen_width = root.winfo_screenwidth()
print(screen_width)

root.overrideredirect(True)
root.wm_resizable(False, False)
#root.wm_resizable(True, True)

menu_handler = MenuHandler(root)

switch_poll_thread = Thread(target = switch_poll)
switch_poll_thread.start()

kill_poll_thread = Thread(target = kill_poll)
kill_poll_thread.start()

#ser = serial.Serial("/dev/ttyS0", 9600) # open uart with 9600 baud rate

root.mainloop()




      
    

    

