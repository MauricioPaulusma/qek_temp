import string
import tkinter
import tkinter.font as tfont
class Button:

    def __init__(self, parent, width: int, height: int, text: string, off_col: string, on_col: string):
        self.active = False
        self.off_col = off_col
        self.on_col = on_col
        self.frame = tkinter.Frame(parent, width=width, height=height)
        self.frame.pack_propagate(0)

        font = tfont.Font(family='Helvatica', size=14)
        self.button = tkinter.Label(self.frame, anchor='nw', text=text, background=off_col, font=font, borderwidth=1, relief=tkinter.SOLID)
        self.button.pack(fill=tkinter.BOTH, expand=1)


    def Activate(self):
        self.active = True
        self.button.configure(background=self.on_col)

    def Deactivate(self):
        self.active = False
        self.button.configure(background=self.off_col)

    def Toggle(self):
        if self.active:
            self.Deactivate()
        else:
            self.Activate()

    def grid(self, row, column, sticky):
        self.frame.grid(row=row, column=column, sticky=sticky)
